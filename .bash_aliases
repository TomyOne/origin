################################################################################
# Taken from https://developer.okta.com/blog/2021/07/07/developers-guide-to-gpg
# configure SSH to use GPG
# export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

# Set an environment variable to tell GPG the current terminal.
# export GPG_TTY=$(tty)

# start gpg-agent, if it isn't started already
# gpgconf --launch gpg-agent
# gpg-connect-agent /bye
# gpg-connect-agent updatestartuptty /bye > /dev/null
# the docs say to use: gpg-connect-agent /bye

# From: https://github.com/drduh/YubiKey-Guide#replace-agents
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
gpg-connect-agent updatestartuptty /bye > /dev/null
################################################################################

export SCREENSHOTS="${HOME}/Pictures/screenshots"
export WALLPAPERS="${HOME}/Pictures/wallpapers"

if [ ! -d ${SCREENSHOTS} ]; then
    mkdir -p ${SCREENSHOTS}
fi
if [ ! -d ${WALLPAPERS} ]; then
    mkdir -p ${WALLPAPERS}
fi

# Server connect
alias skarossh="ssh tom@192.168.50.200"

alias skarosshSync="ssh -R 52698:localhost:52698 tom@192.168.50.200"

alias archupdate="sudo pacman -S archlinux-keyring && sudo pacman -Syu"

yubikeyrefresh () {
    gpgconf --kill gpg-agent
    gpg-connect-agent updatestartuptty /bye
}

alias yubikeystubs='gpg-connect-agent "scd serialno" "learn --force" /bye'
# alias yubikeyrefresh='gpg-connect-agent updatestartuptty /bye'

alias eclipse='GTK_THEME=Breeze\ eclipse eclipse'

alias qelectro='XDG_CURRENT_DESKTOP=GNOME; QT_QPA_PLATFORMTHEME=lxqt; GTK_THEME=Default; qelectrotech'

export FZF_DEFAULT_COMMAND='fd'

export PATH=~/.local/bin:"$PATH"
export PATH="${PATH}":/opt/xtensa-lx106-elf-gcc/bin

export PATH=${PATH}:${HOME}/.local/bin
export PATH=${PATH}:${HOME}/.cargo/bin
export PATH=${PATH}:${HOME}/go/bin

# alias origin='/usr/bin/git --git-dir=$HOME/.origin/ --work-tree=$HOME'
alias origin='$(which git) --git-dir=${HOME}/.origin/ --work-tree=${HOME}'

# include .bash_aliases_spec if it exists
if [ -f ${HOME}/.bash_aliases_spec ]; then
    . ${HOME}/.bash_aliases_spec
fi

function prepend() { while read line; do echo "${1}${line}"; done; }
function append() { while read line; do echo "${line}${1}"; done; }
