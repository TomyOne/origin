#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Aliases
alias ls='ls -hF --color=auto'
alias lr='ls -R'                    # recursive ls
alias ll='ls -la'
alias la='ll -A'
alias lx='ll -BX'                   # sort by extension
alias lz='ll -rS'                   # sort by size
alias lt='ll -rt'                   # sort by date
alias lm='la | more'
alias grep='grep --color=auto'


COL_RED='\[$(tput setaf 1)\]'
COL_GREEN='\[$(tput setaf 2)\]'
COL_YELLOW='\[$(tput setaf 3)\]'
COL_BLUE='\[$(tput setaf 4)\]'
COL_VIOLET='\[$(tput setaf 5)\]'
COL_RESET='\[\e[m\]'

# Source Git prompt
if [ -f /usr/share/git/completion/git-prompt.sh ]; then
    source /usr/share/git/completion/git-prompt.sh
fi

# My custom PS1
case ${HOSTNAME} in
  (ftl) COL_SELECTED=${COL_GREEN};;
  (skaro) COL_SELECTED=${COL_BLUE};;
  (nftl) COL_SELECTED=${COL_YELLOW};;
  (ftl-ntb) COL_SELECTED=${COL_YELLOW};;
  (lftl) COL_SELECTED=${COL_VIOLET};;
  (*)   COL_SELECTED=${COL_RESET};;
esac

export PS1='[\w] $(__git_ps1 " (%s)")'"\n[${COL_SELECTED}\u@\h${COL_RESET}] $> "

# export PS1="[\w]\n[${COL_GREEN}\u@\h${COL_RESET}] $> "
# export PS1='[\u@\h \W$(__git_ps1 " (%s)")]\n\$ '

# Original PS1
# export PS1='[\u@\h \W]\$ '



# Enable bash-completion
if [ -f /usr/share/bash-completion/bash_completion ]; then
    source /usr/share/bash-completion/bash_completion
fi

# hook that will automatically search the official repositories,
# when entering an unrecognized command.
if [ -f /usr/share/doc/pkgfile/command-not-found.bash ]; then
    source /usr/share/doc/pkgfile/command-not-found.bash
fi

# stop logging of repeated identical commands
export HISTCONTROL=ignoredups
shopt -s histappend
export PROMPT_COMMAND='history -a'
export HISTSIZE=10000
export HISTFILESIZE=50000


# include .bash_aliases if it exists
if [ -f ${HOME}/.bash_aliases ]; then
    . ${HOME}/.bash_aliases
fi


# To set trap to intercept a non-zero return code of the last program run
EC() {
    echo -e '\e[1;33m'code $?'\e[m\n'
}
trap EC ERR


# exec fish

# Rust cargo env
if [ -f "$HOME/.cargo/env" ]; then
    . "$HOME/.cargo/env"
fi

# Terraform completion
if [ -f /usr/bin/terraform ];then
    complete -C /usr/bin/terraform terraform
fi

# Bins from Oracle Cloud
if [ -d ${HOME}/bin ]; then
    export PATH=${HOME}/bin:$PATH
fi

[[ -e "${HOME}/lib/oracle-cli/lib/python3.11/site-packages/oci_cli/bin/oci_autocomplete.sh" ]] && source "${HOME}/lib/oracle-cli/lib/python3.11/site-packages/oci_cli/bin/oci_autocomplete.sh"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Ruby
if [ -d ${HOME}/.rbenv/bin ]; then
    export PATH="${HOME}/.rbenv/bin:${PATH}"
    eval "$(rbenv init -)"
fi

# mcux
if [[ -e "${HOME}/workspace/mcu-sdk/mcu-sdk-2.0/bin/bash_completion/sdkgen.sh" ]]; then
  source "${HOME}/workspace/mcu-sdk/mcu-sdk-2.0/bin/bash_completion/sdkgen.sh"
fi
# TODO Change if required
if [ -d "/usr/local/mcuxpressoide/ide/tools" ]; then
    export ARMGCC_DIR=/usr/local/mcuxpressoide/ide/tools
fi
