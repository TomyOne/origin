return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
        local config = require("nvim-treesitter.configs")
        config.setup({
            --ensure_installed = {"javascript", "c", "ruby", "python", "rust", "rst", "bash", "bitbake", "cmake",
            --    "html", "css", "cpp", "devicetree", "dockerfile", "doxygen", "git_config", "git_rebase", "gitattributes",
            --    "gitcommit", "gitignore", "json", "kconfig", "linkerscript", "make", "markdown", "ninja", "nix", "regex",
            --    "yaml"},
            auto_install = true,
            highlight = { enable = true },
            indent = { enable = true },
        })
    end
}

