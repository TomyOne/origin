# Sway Configuration

Credits: https://ubuntusway.com

# Required Apps

## Archlinux

Install packages.

```bash
sudo pacman -Syu alacritty htop cliphist sway swaylock swaybg waybar light wofi mako foot pulsemixer wl-clipboard grim ffmpeg khal playerctl wf-recorder libnotify polkit pcmanfm spice-vdagent kanshi curl pinta blueman network-manager-applet networkmanager nwg-look brightnessctl
yay -S wlogout light nwg-drawer grimshot
```

## Ubuntu

Install alacritty.

```bash
sudo apt update && sudo apt upgrade
sudo add-apt-repository ppa:aslatter/ppa -y
sudo apt install alacritty
```

Install cliphist.

```bash
sudo apt install golang-go
go install go.senan.xyz/cliphist@latest
# Add go bin to to your path
export PATH=${PATH}:${HOME}/go/bin
```

Install other applications.

```bash
sudo apt install sway swaylock swaybg waybar wlogout light wofi mako-notifier foot pulsemixer wl-clipboard grim grimshot ffmpeg khal playerctl wf-recorder ruby-notify lxpolkit pcmanfm spice-vdagent kanshi curl pinta blueman network-manager network-manager-gnome ripgrep brightnessctl
```
How to repair mako-notifier under ubuntu: https://github.com/emersion/mako/issues/257#issuecomment-1638776704

Install nix

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```

Find apps here: [Nix Search](https://search.nixos.org/packages?ref=itsfoss.com)

Install nxg apps

```bash
nix-env -iA nixpkgs.nwg-drawer
sudo ln -s ${HOME}/.nix-profile/bin/nwg-drawer /usr/bin/
```
