#!/bin/sh

case "$1" in
    mute_all_mics_toggle)
        for source in $(pulsemixer --list | grep -ie source- | cut -d' ' -f 3 | cut -d',' -f 1); do
            echo "${source}"
            pulsemixer --id "${source}" --toggle-mute --get-mute
        done
        ;;
    mute_all_mics)
        for source in $(pulsemixer --list | grep -ie source- | cut -d' ' -f 3 | cut -d',' -f 1); do
            pulsemixer --id "${source}" --get-volume > "/tmp/ac-source-last-volume-${source}"
            pulsemixer --id "${source}" --set-volume 0
        done
        ;;
    unmute_all_mics)
        for source in $(pulsemixer --list | grep -ie source- | cut -d' ' -f 3 | cut -d',' -f 1); do
            if [ -f "/tmp/ac-source-last-volume-"${source}"" ]; then
                pulsemixer --id "${source}" --set-volume $(cat "/tmp/ac-source-last-volume-${source}")
            fi
        done
        ;;
    *)
        echo "Usage: $0 {mute_all_mics_toggle|mute_all_mics|unmute_all_mics}"
        exit 2
esac

exit 0
