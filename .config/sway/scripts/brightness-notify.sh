#!/bin/sh

VALUE=$(brightnessctl -m | awk -F, '{print substr($4, 0, length($4)-1)}')
TEXT="Brightness: ${VALUE}%"

notify-send \
    --expire-time 100 \
    --hint string:x-canonical-private-synchronous:brightness \
    --hint "int:value:$VALUE" \
    --hint "int:transient:1" \
    "${TEXT}"
