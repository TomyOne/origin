#!/bin/sh
# https://github.com/swaywm/sway/wiki#clamshell-mode

# Some hacking?
# When lid is closed and sway is refreshed the laptop screen is by default on
# This script is suppose to check if lid is closed and turn off laptop screen and vice-verca
# But without this timeout screen will not be disabled when closed during sway refresh.
# 2 seconds is too low. >:-o
sleep 3
if grep -q open /proc/acpi/button/lid/LID0/state; then
    swaymsg output eDP-1 enable
else
    swaymsg output eDP-1 disable
fi
