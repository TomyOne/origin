#!/usr/bin/env bash
# REF: https://github.com/rileyrg/linux-init#binswaysway-kanshi
# pidof kanshi && echo "kanshi process $(pidof kanshi) already running. Exiting." && exit 0
pkill kanshi &>/dev/null
config="${HOME}/.config/kanshi/config-$(hostname)"
if [ -f  "$config" ]; then
    echo  "kanshi -c $config"
    kanshi -c "$config"
else
    echo "kanshi default config"
    kanshi
fi
