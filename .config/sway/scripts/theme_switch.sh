#!/usr/bin/env bash

FIRST_ARG=${1}
TEMP_THEME_FILE=/tmp/switch_theme_current

ACTION="None"

case ${FIRST_ARG} in

  "toggle")
    ACTION="Toggle"
    ;;

  "light")
    ACTION="Light"
    ;;

  "dark")
    ACTION="Dark"
    ;;

  *)
    ACTION="Ignored"
    ;;
esac


# Change theme for vscode
function vscode_theme {
    theme="Catppuccin Latte"
    replace_path="${HOME}/.config/Code/User/settings.json"
    # TODO: Check if the required theme is available!
    case ${1} in
    "dark")
        theme="Catppuccin Mocha"
        sed -i -e 's/"workbench.colorTheme": ".*"/"workbench.colorTheme": "Catppuccin Mocha"/g' ${replace_path}
        ;;

    "light")
        theme="Catppuccin Latte"
        sed -i -e 's/"workbench.colorTheme": ".*"/"workbench.colorTheme": "Catppuccin Latte"/g' ${replace_path}
        ;;
    *)
        return
        ;;
    esac
}

# Change theme for waybar
function waybar_theme {
    replace_path="${HOME}/.config/waybar/default/style.css"
    case ${1} in
    "dark")
        sed -i -e 's/@import "latte.css.*/@import "mocha.css";/g' ${replace_path}
        ;;

    "light")
        sed -i -e 's/@import "mocha.css.*/@import "latte.css";/g' ${replace_path}
        ;;
    *)
        return
        ;;
    esac

    # Restart waybar
    ${HOME}/.config/sway/scripts/waybar.sh
}

# Change theme for alacritty
function alacritty_theme {
    replace_path="${HOME}/.config/alacritty/alacritty.toml"
    case ${1} in
    "dark")
        sed -i -e 's#"~/.config/alacritty/catppuccin-latte.toml".*#"~/.config/alacritty/catppuccin-mocha.toml"#g' ${replace_path}
        ;;

    "light")
        sed -i -e 's#"~/.config/alacritty/catppuccin-mocha.toml".*#"~/.config/alacritty/catppuccin-latte.toml"#g' ${replace_path}
        ;;
    *)
        return
        ;;
    esac
}

# Change theme for gtk
function gtk_theme {
    replace_path="${HOME}/.local/share/nwg-look/gsettings"
    case ${1} in
    "dark")
        sed -i -e 's/gtk-theme=.*/gtk-theme=Catppuccin-Mocha-Standard-Green-Dark/g' ${replace_path}
        sed -i -e 's/color-scheme=.*/color-scheme=prefer-dark/g' ${replace_path}
        ;;

    "light")
        sed -i -e 's/gtk-theme=.*/gtk-theme=Catppuccin-Latte-Standard-Green-Light/g' ${replace_path}
        sed -i -e 's/color-scheme=.*/color-scheme=prefer-light/g' ${replace_path}
        ;;
    *)
        return
        ;;
    esac

    # TODO: Had to use full path as nix package is not found when this script is started by swaywm
    ${HOME}/.nix-profile/bin/nwg-look -a
    # nwg-look -a
}

function switch_light {
    vscode_theme "light"
    waybar_theme "light"
    alacritty_theme "light"
    gtk_theme "light"
}

function switch_dark {
    vscode_theme "dark"
    waybar_theme "dark"
    alacritty_theme "dark"
    gtk_theme "dark"
}

function toggle_theme {
    curr_theme="None"
    if [[ ! -f ${TEMP_THEME_FILE} ]]; then
        # If no file we switch first to dark
        switch_dark
        echo "dark" > ${TEMP_THEME_FILE}
        return
    fi

    curr_theme=$(cat ${TEMP_THEME_FILE})

    case ${curr_theme} in
        "dark")
            switch_light
            echo "light" > ${TEMP_THEME_FILE}
            PRINT_STR="Light"
            ;;

        "light")
            switch_dark
            echo "dark" > ${TEMP_THEME_FILE}
            PRINT_STR="Dark"
            ;;
        *)
            # Nothing to do here :)
            ;;
        esac
}

PRINT_STR=${ACTION}

case ${ACTION} in

  "Toggle")
    toggle_theme
    ;;

  "Light")
    switch_light
    ;;

  "Dark")
    switch_dark
    ;;

  *)
    # Nothing to do here :)
    ;;
esac

# For debugging
notify-send "Theme Swithed to ${PRINT_STR}" "with arg: ${1}"
