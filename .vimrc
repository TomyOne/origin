set nocompatible

syntax on                          " Set syntax highlight

set number relativenumber          " Show relative line numbers
set visualbell                     " Use visual bell (no beeping)
set showmatch                      " Highlight matching brace
set textwidth=80                   " Line wrap (number of columns)
set colorcolumn=80                 " Show column for 80th character
highlight ColorColumn ctermbg=0 guibg=lightgrey
set list                           " Display blank characters
set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
set cursorline                     " Highlight current line

set tabstop=4                      " Number of spaces per tab
set shiftwidth=4                   " Number of auto-indent spaces
set expandtab                      " Replace tabs with spaces
set smartindent                    " Enable smart-tabs

set hlsearch                       " Highlight all search results
set smartcase                      " Enable smart-case search
set ignorecase                     " Always case-insensitive
set incsearch                      " Searches for strings incrementally

set wrap linebreak nolist
set noswapfile                     " Don't create a swap file
set nobackup
set undodir=~/.vim/undodir
set undofile
set softtabstop=0 noexpandtab
